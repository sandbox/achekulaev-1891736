<?php

/**
 * @file
 * Provide views data and handlers for the RNM Taxonomy Workbench module.
 */

/**
 * Implements hook_views_data().
 */
function rnm_taxonomy_workbench_views_data() {


  $data['taxonomy_term_data']['table']['join'] = array(
    'rnm_taxonomy_workbench_term_history' => array(
      'left_field' => 'tid',
      'field' => 'tid',
      'type' => 'INNER',
    ),
  );


  $data['rnm_taxonomy_workbench_term_history']['table']['group']  = t('Taxonomy term revision');
  $data['rnm_taxonomy_workbench_term_history']['table']['base'] = array(
    'field' => 'revision_id',
    'title' => t('Taxonomy term workbench'),
    'help' => t('Taxonomy term workbench is a history of changes to taxonomy term states.'),
  );

  // For other base tables, explain how we join.
  $data['rnm_taxonomy_workbench_term_history']['table']['join'] = array(
    'taxonomy_term_data' => array(
      'left_field' => 'revision_id',
      'field' => 'revision_id',
    ),
  );


  // Uid field for user revision.
  $data['rnm_taxonomy_workbench_term_history']['uid'] = array(
    'title' => t('User'),
    'help' => t('Relate a taxonomy term workbench history to the user who created the revision.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'base field' => 'uid',
      'label' => t('Revision user'),
    ),
  );

  $data['rnm_taxonomy_workbench_term_history']['revision_id'] = array(
    'title' => t('Revision id'),
    'help' => t('The revision ID of the taxonomy term revision.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    // Information for accepting a nid as an argument.
    'argument' => array(
      'handler' => 'views_handler_argument_taxonomy_term_revision_id',
      'click sortable' => TRUE,
      'numeric' => TRUE,
    ),
    // Information for accepting a uid as a filter.
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    // Information for sorting on a uid.
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'terms',
      'base field' => 'revision_id',
      'title' => t('Taxonomy term'),
      'label' => t('Get the actual term from a taxonomy term revision.'),
    ),
  );


  $data['rnm_taxonomy_workbench_term_history']['tid'] = array(
    'title' => t('Tid'),
    'help' => t('The tid field of the taxonomy term workbench term history table'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'terms',
      'base field' => 'tid',
      'title' => t('Taxonomy term'),
      'label' => t('Get all revisions from an user.'),
    ),
  );


  $data['rnm_taxonomy_workbench_term_history']['stamp'] = array(
    'title' => t('State changed date'),
    'help' => t('The date the taxonomy term state was created.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['rnm_taxonomy_workbench_term_history']['state'] = array(
    'title' => t('State'),
    'help' => t('Content moderation state of the term revision.'),
    'filter' => array(
      'handler' => 'workbench_moderation_handler_filter_state',
    ),
    'field' => array(
      'handler' => 'rnm_taxonomy_workbench_moderation_handler_field_state',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
      'name table' => 'workbench_moderation_states',
      'name field' => 'label',
    ),
  );

  $data['rnm_taxonomy_workbench_term_history']['from_state'] = array(
    'title' => t('From state'),
    'help' => t('Previous content moderation state of the term revision.'),
    'filter' => array(
      'handler' => 'workbench_moderation_handler_filter_state',
    ),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
      'name table' => 'workbench_moderation_states',
      'name field' => 'label',
    ),
  );

  $data['rnm_taxonomy_workbench_term_history']['published'] = array(
    'title' => t('Published'),
    'help' => t('Whether or not this revision is published.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Published'),
      'type' => 'yes-no',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['rnm_taxonomy_workbench_term_history']['current'] = array(
    'title' => t('Current'),
    'help' => t('Whether or not this is the current revision.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Current'),
      'type' => 'yes-no',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );


  // $data['taxonomy_term_data_revision']['revert_revision'] = array(
  //   'field' => array(
  //     'real field' => 'tid',
  //     'title' => t('Revert link'),
  //     'help' => t('Provide a simple link to revert to the revision.'),
  //     'handler' => 'views_handler_field_taxonomy_term_revision_link_revert',
  //   ),
  // );

  // $data['taxonomy_term_data_revision']['delete_revision'] = array(
  //   'field' => array(
  //     'real field' => 'tid',
  //     'title' => t('Delete link'),
  //     'help' => t('Provide a simple link to delete the taxonomy term revision.'),
  //     'handler' => 'views_handler_field_taxonomy_term_revision_link_delete',
  //   ),
  // );

  return $data;
}

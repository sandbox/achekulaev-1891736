<?php

/**
 * @file
 * UI pages for revisions, similar with pages from node.pages.inc.
 */

/**
 * Generate an overview table of older revisions of a taxonomy term.
 *
 * @param $viewed_term
 *  The term for which the revisions are listed.
 */
function rnm_taxonomy_workbench_revision_overview($viewed_term) {
  drupal_set_title(t('Revisions for %title', array('%title' => $viewed_term->name)), PASS_THROUGH);

  $header = array(t('Revision'), array('data' => t('Operations'), 'colspan' => 2));

  $revisions = rnm_taxonomy_workbench_list($viewed_term);

  $rows = array();
  $revert_permission = FALSE;
  $delete_permission = FALSE;
  if (user_access('revert taxonomy term revisions') || user_access('administer taxonomy')) {
    $revert_permission = TRUE;
  }
  if (user_access('delete taxonomy term revisions') || user_access('administer taxonomy')) {
    $delete_permission = TRUE;
  }

  $published = drupal_placeholder(t('published revision'));

  foreach ($revisions as $revision) {
    $row = array();
    $operations = array();
    if ($revision->revision_id == $viewed_term->revision_id) {
      $row[] = array(
        'data' => t('!date by !username', array(
          '!date' => l(format_date($revision->timestamp, 'short'), "taxonomy/term/$viewed_term->tid"),
          '!username' => theme('username', array('account' => user_load($revision->uid))) . ($revision->published ?  ' ' . $published : ''),
        )) . (($revision->log != '') ? '<p class="revision-log">' . filter_xss($revision->log) . '</p>' : ''),
        'class' => array('revision-current'),
      );
      $operations[] = array(
        'data' => drupal_placeholder(t('current revision')),
        'class' => array('revision-current'),
        'colspan' => 2,
      );
    }
    else {
      $row[] = t('!date by !username', array(
        '!date' => l(format_date($revision->timestamp, 'short'), "taxonomy/term/$viewed_term->tid/revisions/$revision->revision_id/view"),
        '!username' => theme('username', array('account' => user_load($revision->uid))) . ($revision->published ?  ' ' . $published : ''),
      )) . (($revision->log != '') ? '<p class="revision-log">' . filter_xss($revision->log) . '</p>' : '');
      if ($revert_permission) {
        $operations[] = l(t('revert'), "taxonomy/term/$viewed_term->tid/revisions/$revision->revision_id/revert");
      }
      else {
        // Empty row for theming purposes.
        $operations[] = "";
      }
      if ($delete_permission) {
        $operations[] = l(t('delete'), "taxonomy/term/$viewed_term->tid/revisions/$revision->revision_id/delete");
      }
      else {
        // Empty row for theming purposes.
        $operations[] = "";
      }
    }
    $rows[] = array_merge($row, $operations);
  }

  $build['taxonomy_term_revisions_table'] = array(
    '#theme' => 'table',
    '#rows' => $rows,
    '#header' => $header,
  );

  return $build;
}


/**
 * Menu callback -- ask for confirmation of term deletion
 */
function rnm_taxonomy_term_delete_confirm($form, &$form_state, $term) {
  $form['#term'] = $term;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['tid'] = array('#type' => 'value', '#value' => $term->tid);
  return confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $term->name)),
    'taxonomy/term/' . $term->tid,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Execute term deletion
 */
function rnm_taxonomy_term_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    taxonomy_term_delete($form_state['values']['tid']);
    watchdog('taxonomy', 'deleted %title.', array('%title' => $form['#term']->name));
    drupal_set_message(t('%title has been deleted.', array('%title' => $form['#term']->name)));
  }

  $form_state['redirect'] = '<front>';
}
